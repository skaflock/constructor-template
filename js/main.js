(function(){
  'use strict';
  /* jslint browser: true */
  /* global $, jQuery */

  /* document ready */
  $(function(){});

  /* window load */
  $(window).on('load', function(){});
}());
